Working with Service Programs:

With the SubMenu, Hot Keys and FunctionKeys from above you are able to load and execute Service Programs coded in Basic!. 
If a Sub Menu item ends with ".bas" it is linked to an equal named Basic! program in the source sub folder Service_Programs.
If a Basic program in source/Service-Programs called, the current program file name and the selection/cursor will be send like a RUN command.

The Service_Programs directory is direct behind the source dirctory.

	. . . . . /rfo-basic/source/Service Programs/ 

The structure is:

	Service Programs
	
		CodeService.bas
		
		cs_data
		
			data
			
		Manual_Help.bas
		
		mh_data
		
			data
			
		. . .
		
		. . .

See also RUN