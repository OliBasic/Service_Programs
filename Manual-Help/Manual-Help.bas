
! ##$="/storage/emulated/0/rfo-basic/source/---------- Asset.bas?start=0?end=0?com.rfo.basicOli"


REM Example for a Manual-Help.bas

FN.DEF searchExeptions$(search$)
 search$ = LOWER$( search$)
 ! search$ = REPLACE$( search$, ",", "")
 search$ = REPLACE$( search$, "then", "if")
 search$ = REPLACE$( search$, "elseif", "if")
 search$ = REPLACE$( search$, "else", "if")
 search$ = REPLACE$( search$, "endif", "if")
 search$ = REPLACE$( search$, "repeat", "while")
 search$ = REPLACE$( search$, "until", "do")
 IF LEFT$(search$,2) = "do" | LEFT$(search$,3) = ">do"
  search$ = REPLACE$( search$, "do", "do / until")
 ENDIF
 IF LEFT$(search$,2) = "to" | LEFT$(search$,3) = ">to"
  search$ = REPLACE$( search$, "to", "for")
 ENDIF
 IF LEFT$(search$,5) = "print" | LEFT$(search$,6) = ">print"
  search$ = REPLACE$( search$, "print", "print {")
 ENDIF
 search$ = REPLACE$( search$, "step", "for")
 search$ = REPLACE$( search$, "next", "for")
 IF LEFT$(search$,3) = "for" | LEFT$(search$,4) = ">for"
  search$ = REPLACE$( search$, "for", "for - to - step /")
 ENDIF
 search$ = REPLACE$( search$, "atan", "atan(")
 search$ = REPLACE$( search$, "app.load", "app.installed")

 search$ = REPLACE$( search$, "🔙","automatic")
 FN.RTN search$
FN.END
FN.DEF getRGB$(c$)
 FN.RTN INT$(HEX(MID$(c$,2,2))) + "," + INT$(HEX(MID$(c$,4,2))) + "," + INT$(HEX(RIGHT$(c$,2)))
FN.END 
FN.DEF setcolor(c$)
 GR.COLOR 255, ~
 HEX(MID$(c$,2,2)), ~
 HEX(MID$(c$,4,2)), ~
 HEX(RIGHT$(c$,2))
FN.END 
FN.DEF getColor$()
 CLIPBOARD.GET c$ %
 IF LEFT$(C$, 1) <> "#" THEN c$ = "#" + c$
 C$ = UPPER$(c$)
 rgb$ = INT$(HEX(MID$(c$,2,2))) + "," + INT$(HEX(MID$(c$,4,2))) + "," + INT$(HEX(RIGHT$(c$,2)))
 ARRAY.LOAD colo$[], getRGB$(c$), c$, "255," + getRGB$(c$), REPLACE$(c$,"#","#FF"),"CHANCEL"
 DIALOG.SELECT sel, colo$[] 
 IF sel & sel < 5 THEN cData$ = colo$[sel]
 FN.RTN cData$
FN.END 
FN.DEF ViewFile(fileName$, packageName$)

 LIST.CREATE S, commandListPointer
 LIST.ADD commandListPointer~
 "new Intent(Intent.ACTION_VIEW);" ~
 !"setPackage("+CHR$(34)+packageName$+CHR$(34)+");" ~
 "setDataAndType("+CHR$(34)+fileName$+CHR$(34)+","+CHR$(34)+ "application/pdf"+CHR$(34)+");" ~
 "addCategory(Intent.CATEGORY_DEFAULT);" ~
 "addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);" ~
 "addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);" ~
 "EOCL"
 LIST.CREATE S, resultListPointer
 LIST.ADD resultListPointer~
 "EORL"
 BUNDLE.PL appVarPointer,"_CommandList",commandListPointer

 APP.SAR appVarPointer

 FN.RTN appVarPointer

FN.END
FN.DEF ReLaunch(basEngine$, basProgramPath$, mode, mStart, mEnd)
 WAKELOCK 5
 eMode$ = ""
 IF mode > 0
  eMode$ = "_Editor"
  IF mStart > -1 & mEnd > -1 THEN eMode$ = eMode$ + "?start=" + INT$(mStart) + "?end=" + INT$(mEnd)   
 ENDIF
 LIST.CREATE S, commandListPointer
 LIST.ADD commandListPointer~
 "new Intent(Intent.ACTION_MAIN);" ~
 "setData("+ CHR$(34) + basProgramPath$ + CHR$(34) +");" ~
 "new ComponentName("+ CHR$(34) + basEngine$ + CHR$(34) + ","+CHR$(34)+ basEngine$ + ".Basic" + CHR$(34)+");" ~
 "addCategory(Intent.CATEGORY_DEFAULT);" ~
 "putExtra("+ CHR$(34) + "_BASIC!" + CHR$(34) + ","+CHR$(34)+ eMode$ + CHR$(34)+");" ~ %Starts program in Editor mode, if eMode$ = "_Editor"!
 "addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);" ~
 "addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);" ~
 "addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);" ~
 "addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);" ~
 "EOCL"
 BUNDLE.PL appVarPointer,"_CommandList",commandListPointer
 APP.SAR appVarPointer
FN.END
FN.DEF InitReLaunch(objectValues)
 BUNDLE.GET objectValues, "basProgramPath", basProgramPath$
 BUNDLE.GET objectValues, "startSelection", startSelection
 BUNDLE.GET objectValues, "endSelection", endSelection
 BUNDLE.GET objectValues, "package", basEngine$
 mode = 1
 CALL ReLaunch(basEngine$, basProgramPath$, mode, startSelection, endSelection)
FN.END
FN.DEF GetBasObjectValues (objectValues)
 GLOBALS.FNIMP ##$ % On start a line started with ##$ is added by Basic!. See also the RUN command.
 SPLIT.ALL spRes$[], ##$, "\\?"% ? is a control delimiter
 ARRAY.LENGTH al, spRes$[]
 IF al > 0
  BUNDLE.PUT objectValues, "basProgramPath", spRes$[1]
  BUNDLE.PUT objectValues, "startSelection", VAL(MID$(spRes$[2], 7))
  BUNDLE.PUT objectValues, "endSelection", VAL(MID$(spRes$[3], 5))
  BUNDLE.PUT objectValues, "package", MID$(spRes$[4], 9)
 ENDIF
FN.END

!*** Main Part ***
WAKELOCK 1
BUNDLE.CREATE objectValues
GetBasObjectValues (objectValues)

!* The Selection Parameters 
!* If startSelection = endSelection THEN it is the cursor position
!* The position values start with 0 (before the first sign)
BUNDLE.GET objectValues, "startSelection", startSelection
BUNDLE.GET objectValues, "endSelection", endSelection

!? startSelection
!? endSelection

CLIPBOARD.GET mClipData$ % Get automatically created text from the selected Basic source contents
!? mClipData$

!*** Insert your code here! ***
!sel = -6000
!DIALOG.MESSAGE "Do your code here!", "In 6 seconds we will return!", sel

FILE.ROOT dPath$, "_source"
sPath$ = "file://" + dPath$ + "/Sample_Programs/"
dPath$ = "file://" + dPath$ + "/Service_Programs/mh_data/"
commandsBas$ = dPath$ + "f01_commands.bas"
wordsOfInter$ = dPath$ + "wordsOfInter.txt"
manualTxt$ = dPath$ + "De_Re_BASIC!1.91.txt"
manualHtml$ = dPath$ +  "De_Re_BASIC!1.91.html"
manualPdf$ = dPath$ +  "De_Re_BASIC!1.91.pdf"
appendixTxt$ = dPath$ +  "OliBasic XXIII Appendix 2018-03-31.txt"
appendixHtml$ = dPath$ +  "OliBasic XXIII Appendix 2018-03-31.html"
appendixPdf$ = dPath$ +  "OliBasic XXIII Appendix 2018-03-31.pdf"
gwLibHtml$ = dPath$ +  "cheatsheet.html"

search$ = TRIM$(mClipData$)
!search$ = "Gw_"

IF search$ = "" THEN search$ = "*"
IF search$ = "*" THEN search$ = "" % List all commands 
IF IS_IN("gw_", LOWER$(search$)) <> 1 % If not a GWlib Command

 GRABFILE mGrabM$, commandsBas$ % Load commands
 mem$ = mGrabM$
 FILE.EXISTS ok, wordsOfInter$
 IF ok
  GRABFILE mGrabM$, commandsBas$ % Load word of interest
  mGrabM$ = mem$ + mGrabM$
 ENDIF
 ? Is_in ("ABS", mGrabM$)
 mGrabM$ = MID$(mGrabM$, Is_in ("ABS", mGrabM$)) % Start where the commands begin
 SPLIT.ALL mGrabMLines$[], mGrabM$, "\n"
 mGrabM$ = "" % FreeMem
 DEBUG.ON
 !debug.dump.array mGrabMLines$[]


 LIST.CREATE S, lCommands
 LIST.ADD.ARRAY lCommands, mGrabMLines$[]

 LIST.CREATE S, lCommandMatches
 !* Search in the commands / WOIs
 LIST.MATCH , lCommandMatches, lCommands, search$, ,,,"_Starts_With_IgnoreCase"
 LIST.INSERT lCommandMatches, 1, "Color Picker Over Clipboard" % open a color picker
 LIST.INSERT lCommandMatches, 2, "🔙     Using, try Click, DoubleClick and LongClick, " % return
 ! DEBUG.DUMP.LIST lCommandMatches
 BUNDLE.PUT selLayout, "_TextBackgroundColor", "255,216,193"
 BUNDLE.PUT selLayout, "_DividerColor", "255,0,0,0"
 BUNDLE.PUT selLayout, "_DividerHeight", 2
 !* First command selection
 SELECT sel, lCommandMatches , "Commands", "Commands / Words of Interest", pressType, selLayout
 IF sel  = 0 THEN GOTO ENDLabel
 IF sel = 1 & pressType <> 1
  BROWSE "https://www.w3schools.com/colors/colors_picker.asp" % Maybe delete "colors_picker.asp" to get the full stuff.
  !* Halt execution with a dialog.massage command
  DIALOG.MESSAGE "Color code as HEX","is in the clipboard, return", sel, "OK", "Chancel"
  IF sel = 1 THEN mClipData$ = getColor$()
  GOTO ENDLabel
 ELSEIF sel = 2 & pressType = 0
  GOTO ENDLabel
 ELSEIF sel > 2 & pressType = 0
  LIST.GET lCommandMatches, sel, mClipData$
  mClipData$ = LEFT$(mClipData$, IS_IN(",", mClipData$, -1)-1) % First -1, because searching backwards
  !PRINT mClipData$ 
  GOTO EndLabel
 ELSEIF pressType = 1 % LongPress
  ! Option menu
  ARRAY.LOAD options$[], "OliBasic at GitLab", "Manual RFO-Basic", ~
  "Appendix OliBasic", "WIKI", "WIKI search", "Forum", "Forum search", "Forum Google search", "-----", "FINISH"
  DO
   dSel = 0
   DIALOG.SELECT dSel, options$[] % Also used as a breakpoint, so it does not run forward in an uncontrolled manner
   IF dSel = 1
    BROWSE "https://gitlab.com/OliBasic/"
   ELSEIF dSel = 2
    ViewFile("file://"+manualPdf$, "")
   ELSEIF dSel = 3
    ViewFile( "file://"+appendixPdf$, "")
   ELSEIF dSel = 4
    BROWSE "https://rfobasic.miraheze.org/wiki/"
   ELSEIF dSel = 5
    INPUT "Add something , if you want", search$, search$
    BROWSE "https://www.google.com/?#q="+search$+" site:rfobasic.miraheze.org/wiki/"
   ELSEIF dSel = 6
    BROWSE "https://www.tapatalk.com/groups/rfobasic/"
   ELSEIF dSel = 7
    BROWSE "https://www.tapatalk.com/groups/rfobasic/search.php"
   ELSEIF dSel = 8
    INPUT "Add something , if you want", search$, search$
    BROWSE "https://www.google.com/?#q="+search$+" site:tapatalk.com/groups/rfobasic/"
   ELSE 
    dSel = 0
   ENDIF
  UNTIL dSel = 0
  PAUSE 500
  GOTO EndLabel
 ENDIF


 search$ = searchExeptions$(search$)

 LIST.GET lCommandMatches, sel, search$
 PRINT "search$", search$
 manual = 1


 IF ENDS_WITH(",", TRIM$(search$)) THEN manual = 0
 search$ = REPLACE$( search$, "[ (<]", "@", "_RegEx_First")
 search$ = LEFT$(search$, IS_IN("@", search$)-1)

 IF manual < 1
  FILE.EXISTS ok, appendixTxt$ % Last before End
  GRABFILE mGrabM$, appendixTxt$
 ELSE
  FILE.EXISTS ok, manualTxt$ % End
  GRABFILE mGrabM$, manualTxt$
  wb = IS_IN("is Latin for", mGrabM$)
  we = IS_IN("program uses an Intent to start", mGrabM$, wb)
  mGrabM$ = MID$(mGrabM$, wb, we-wb)
  PRINT wb , we, "txt"
 ENDIF
 SPLIT.ALL mGrabMLines$[], mGrabM$, "\n"
 mGrabM$ ="" %FreeMem
 LIST.CLEAR lCommands
 LIST.ADD.ARRAY lCommands, mGrabMLines$[]
 !* Search for Command in the Appendix Text File
 LIST.MATCH , lCommandMatches, lCommands, search$, ,,,"_Starts_With_IgnoreCase"
 LIST.SIZE lCommandMatches, ls

 sel = 1
 search$ = searchExeptions$(search$)
 ?search$
 IF manual < 1
  GRABFILE mGrabM$, appendixHtml$ %Load Appendix as html
 ELSE 
  GRABFILE mGrabM$, manualHtml$ %Load manual as html
 ENDIF
 SPLIT.ALL mGrabMLines$[], mGrabM$, "\n"
 mGrabM$ =""

 ? "manual",manual
 ! mGrabM$ = left$ (mGrabM$, 12000) : ? mGrabM$ : END

 LIST.CLEAR lCommands
 LIST.ADD.ARRAY lCommands, mGrabMLines$[]
 LIST.CREATE N, lNumbered1
 search$ = ">" + REPLACE$(search$, ",", "")
 searchmem$ = search$
 fin = 0
 DO %Compare with the html file
  search$ = searchmem$
  fin = fin + 1
  IF fin = 2 THEN % Second round for the case, that the command is in th front of line without a >.
   LIST.CLEAR lCommands
   LIST.ADD.ARRAY lCommands, mGrabMLines$[]
   search$ = REPLACE$(search$, ">", "")
  ENDIF
  PRINT search$, fin
  LIST.MATCH lNumbered1, lCommandMatches, lCommands, search$, ,,,"_Is_In_IgnoreCase"
  DEBUG.DUMP.LIST lNumbered1
  LIST.GET lNumbered1, 1, w1
  ? mGrabMLines$[w1]
  SWAP lCommandMatches, lCommands
  search$ = CHR$(9) + "<h" %Manual
  IF manual < 1 THEN search$ = "<h" %Appendix

  LIST.CREATE N, lNumbered2

  LIST.MATCH lNumbered2, lCommandMatches, lCommands, search$, ,,,"_Is_In_IgnoreCase"
  LIST.SIZE lNumbered2, ls
  ?"ls1: ",ls
  IF ls = 0 THEN LIST.MATCH lNumbered2, lCommandMatches, lCommands, "</h2>", ,,,"_Is_In_IgnoreCase"
  LIST.SIZE lNumbered2, ls
  ?"ls2: ",ls
  IF ls = 0 THEN LIST.MATCH lNumbered2, lCommandMatches, lCommands, "</h3>", ,,,"_Is_In_IgnoreCase"
  LIST.SIZE lNumbered2, ls
  ?"ls3: ",ls
  IF ls = 0 THEN LIST.MATCH lNumbered2, lCommandMatches, lCommands, "</h4>", ,,,"_Is_In_IgnoreCase"
  IF ls > 0 THEN fin=2
 UNTIL fin = 2
 DEBUG.DUMP.LIST lNumbered2
 !* Exeption handling
 IF ls = 0 
  descr$ = "Manual"
  mClipData$ = "Search for: " + searchmem$ + "\n in the " + descr$ + "\n Last search string: " + search$ + "\n in loop: " + INT$(fin) + "\n This result is in the clipboeard now! "
  IF manual < 1 THEN descr$ = "Appendix"
  DIALOG.MESSAGE "Command Not Found in the Html File!", mClipData$ , sel, "OK"
  GOTO ENDLabel
 ENDIF

 LIST.GET lNumbered2, sel, w2
 LIST.GET lNumbered1, w2, w1
 !?"w1: ", w1
 ARRAY.COPY mGrabMLines$[w1,1000], DestinationArray$[]
 text$ = "<script type='text/javascript'>"
 text$ = text$ + "function doDataLink(data) {Android.dataLink('DAT:TRANS:' + myDIV.textContent);}"
 text$ = text$ + "</script>"
 IF manual THEN  header$ = "MANUAL <b>RFO-Basic 1.91</b>" : ELSE  header$ = "APPENDIX <b>OliBasic</b>"
 header$ = header$ + "&#160&#160&#160<input type='button' value='Translate' onClick='doDataLink()'/>"
 div1$ ="<div id='myDIV' style='margin: 0 auto; width:100%; height:100%;'>"
 div2$ = "</div>"
 JOIN DestinationArray$[], mGrabM$ , "\n"
 mGrabM$ = text$ + header$ + div1$ + mGrabM$ + div2$
ELSE
 manual = 2 % GWlib CheetSheet
 GRABFILE mGrabM$, gwLibHtml$
ENDIF

IF manual = 1 THEN  mGrabM$ = "<body style='background-color:#E6E6FA'>" + mGrabM$
IF manual = 0 THEN  mGrabM$ = "<body style='background-color:#FAE6E6'>" + mGrabM$
HTML.OPEN
HTML.LOAD.STRING mGrabM$
DEVICE. Locale locFull$
loc$ = LEFT$(locFull$, 2)
urlFirst$ = "https://translate.google.com/?hl=" + loc$ + "#en/" + loc$ + "/"

xnextUserAction:
DO
 HTML.GET.DATALINK data$
UNTIL data$ <> ""
type$ = LEFT$(data$, 4)
data$ = MID$(data$,5)
SW.BEGIN type$
  ! Back Key hit.
  ! if we can go back then do it
 SW.CASE "BAK:"
  ! PRINT "BACK key: " + data$
  IF data$ = "1" THEN HTML.GO.BACK ELSE GOTO ENDLabel
  SW.BREAK

  ! A hyperlink was clicked on
 SW.CASE "LNK:"
  PRINT "Hyperlink selected: "+ data$
  IF RIGHT$(data$,7)<>"sign_in" THEN HTML.LOAD.URL data$
  SW.BREAK
  ! User data returned
 SW.CASE "DAT:"
  IF LEFT$(data$, LEN("DAT:TRANS:") ) = "DAT:TRANS:"
   data$ = MID$(data$, LEN("DAT:TRANS:")+1)
   enc$ =  ENCODE$("URL", ,data$)
   enc$ = Replace$ (enc$, "%0A", "")
   HTML.LOAD.URL urlFirst$ + enc$
  ENDIF

  ! Check for Exit
  IF data$ = "Exit"
   PRINT "User ended demo."
   HTML.CLOSE
   gogo ENDLabel
  ENDIF
  SW.BREAK
SW.END

GOTO xnextUserAction
DO 
UNTIL 0

ENDLabel:

CLIPBOARD.PUT mClipData$

!* Set the new cursor/selection position(s)
BUNDLE.PUT objectValues, "startSelection", startSelection
BUNDLE.PUT objectValues, "endSelection", endSelection

InitReLaunch(objectValues)
EXIT

ONBACKKEY:
HTML.CLOSE
CLIPBOARD.GET mClipData$
GOTO E